# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.6](https://github.com/graytonio/MyInventory/compare/v0.0.5...v0.0.6) (2021-03-10)

### [0.0.5](https://github.com/graytonio/Atlas/compare/v0.0.4...v0.0.5) (2021-03-10)

### [0.0.4](https://github.com/graytonio/Atlas/compare/v0.0.3...v0.0.4) (2021-03-10)

### [0.0.3](https://github.com/graytonio/Atlas/compare/v0.0.2...v0.0.3) (2021-03-10)

### 0.0.2 (2021-03-10)
